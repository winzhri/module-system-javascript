import { Table } from "module.js";

const tableLibrary = new Table({
  columns: [`Name`, `Email`, `Address`],
  Data: [
    [`Wina`, `winaaz@gmail.com`, `Bandung`],
    [`Azha`, `azha123@gmail.com`, `Jakarta`],
    [`Nana`, `nana0813@gmail.com`, `Jakarta`]
  ]
});

const app = document.getElementById("app");
tableLibrary.render(app);
